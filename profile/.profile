export BROWSER="/usr/bin/chromium"
export MOZ_ENABLE_WAYLAND=1
export PATH="${HOME}/bin:${HOME}/.local/bin:/usr/local/bin:/usr/sbin:${PATH}"
export GOIL_TEMPLATES="${HOME}/cours/trampoline/goil/templates"
export VIPER_PATH="${HOME}/cours/trampoline/viper/viper"
